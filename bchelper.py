from selenium import webdriver
import sys
import urllib.request 
import os


def track_to_poppler(browser, link_to_track):
	browser.get(link_to_track)

	browser.find_element_by_class_name('playbutton').click()
	audio_elem = browser.find_element_by_tag_name('audio')
	src = audio_elem.get_attribute('src')
	track_name = browser.find_element_by_id('name-section').find_element_by_class_name('trackTitle').text

	return  track_name, src

##########################################
# Grab all the album metadata including 
# the album name, artist, and links to
# the individual tracks.
##########################################
def grab_album_metadata(browser, link_to_album):
	browser.get(link_to_album)
	track_links = []
	# dict that will contain all the metadata
	album_metadata = {}
	metadata = browser.find_element_by_id('name-section')
	#############################################
	album_metadata['album_name'] = metadata.find_element_by_class_name('trackTitle').text
	album_metadata['artist_name'] = metadata.find_element_by_tag_name('a').text
	tracks = browser.find_elements_by_class_name("title-col")
	# grab each track link. This is not the download link
	# but just the bandcamp page for the track
	for track in tracks:
		a_tag = track.find_element_by_tag_name('a')
		track_links.append(a_tag.get_attribute('href'))
	album_metadata['track_links'] = track_links
	return album_metadata

################################################
# 'poppler' shows up on the download links
# for tracks. We use it to refer to specifically
# to the download link rather than just to the 
# bandcamp page link.
###############################################
def album_to_popplers(browser, list_of_links):
	dict_of_tracks = {}
	# collect the poppler links for all the tracks
	for link in list_of_links:
		track_name, poppler = track_to_poppler(browser, link)
		dict_of_tracks[track_name] = poppler
	return dict_of_tracks

def download_album(dict_of_tracks, dir_name):
	os.mkdir(dir_name)
	for track_name, poppler in dict_of_tracks.items():
		file_name = "{}/{}.mp3".format(dir_name,track_name)
		urllib.request.urlretrieve(poppler, file_name)

if __name__ == '__main__':
	browser = webdriver.Firefox(executable_path='geckodriver')
	metadata = grab_album_metadata(browser, sys.argv[1])
	track_links = metadata['track_links']
	# make a folder to save the tracks into
	dir_name = '{} - {}'.format(metadata['artist_name'], metadata['album_name'])
	dict_of_tracks = album_to_popplers(browser, track_links)
	browser.close()
	download_album(dict_of_tracks, dir_name)
	
